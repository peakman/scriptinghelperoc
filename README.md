# README #

This repository contains an Xcode project to facilitate the use of ScriptingBridge within Swift. There's little function here but the framework includes Numbers.h and Pages.h because that's the only way I could find to make them available to a Swift framework.

As of 24th July 2014 the framework was set up on Xcode6 Beta 3

This framework needs to be used in conjunction with the Swift framework ScriptingHelper.  That's where the real work is done.