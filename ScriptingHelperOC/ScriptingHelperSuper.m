//
//  ScriptingHelper.m
//  ScriptingHelperOC
//
//  Created by Steve Clarke on 07/06/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

#import "ScriptingHelperSuper.h"
#import <ScriptingBridge/ScriptingBridge.h>
#import "Numbers.h"

void setRangeFormat(id range, NumbersNMCT format) {
    NumbersRange * rng = (NumbersRange *)range;
    NSAppleEventDescriptor * aed = [NSAppleEventDescriptor descriptorWithEnumCode: format];
    [rng setFormat: aed];
}

NSString * missingSBValue () {
    return @"__MISSING__VALUE__";
}

NSMutableArray *  valuesForRange( SBElementArray * range) {
    __block NSMutableArray * values = [[NSMutableArray alloc] init  ];
    __block id value = nil;
    [range enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        value = [[(NumbersCell *)obj value] get] ;
        if (value) {
            [values addObject: value];
        } else {
            [values addObject: missingSBValue()];
        }
    }];
    return values;
}

NSString * nameOfSelectionRangeOf(id numbersTable ) {
    return [[(NumbersTable *)numbersTable selectionRange] name];
}

@implementation ScriptingHelperSuper


@end
