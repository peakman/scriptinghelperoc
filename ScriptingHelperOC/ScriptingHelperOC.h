//
//  ScriptingHelperOC.h
//  ScriptingHelperOC
//
//  Created by Steve Clarke on 07/06/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ScriptingBridge/ScriptingBridge.h>
#import "Numbers.h"
#import "ScriptingHelperSuper.h"


//! Project version number for ScriptingHelperOC.
FOUNDATION_EXPORT double ScriptingHelperOCVersionNumber;

//! Project version string for ScriptingHelperOC.
FOUNDATION_EXPORT const unsigned char ScriptingHelperOCVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ScriptingHelperOC/PublicHeader.h>


