//
//  ScriptingHelper.h
//  ScriptingHelperOC
//
//  Created by Steve Clarke on 07/06/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ScriptingBridge/ScriptingBridge.h>
#import "Numbers.h"

NSArray *  valuesForRange( SBElementArray * range);
void setRangeFormat(id range, NumbersNMCT format);
NSString * missingSBValue();
NSString * nameOfSelectionRangeOf(id numbersTable ) ;

@interface ScriptingHelperSuper : NSObject

@end
